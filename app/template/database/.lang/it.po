#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: database 3.13.90\n"
"POT-Creation-Date: 2020-11-18 18:25 UTC\n"
"PO-Revision-Date: 2019-05-18 17:09 UTC\n"
"Last-Translator: Gianluigi Gradaschi <bagonergi@gmail.com>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1
msgid "Database application"
msgstr "Applicazione database"

#: .project:2
msgid "A graphical application accessing a database."
msgstr "Un'applicazione grafica che accede a un database."
