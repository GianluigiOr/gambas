#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2002-11-01 04:27+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1
msgid "Object manipulation example"
msgstr "Beispiel für Objekt-Manipulation"

#: FStart.class:29
msgid "You need to create the Thing first!"
msgstr "Sie müssen das Objekt erst erstellen!"

#: FStart.form:12
msgid "Object "
msgstr "-"

#: FStart.form:18
msgid "create the Thing !"
msgstr "Objekt erstellen"

#: FStart.form:23
msgid "check the Thing !"
msgstr "Objekt prüfen"

#: FStart.form:33
msgid "destroy the Thing !"
msgstr "Objekt löschen"

#: FStart.form:38
msgid " by juergen@zdero.com"
msgstr "-"

